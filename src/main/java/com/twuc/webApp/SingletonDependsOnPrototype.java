package com.twuc.webApp;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class SingletonDependsOnPrototype {
	private PrototypeDependent prototypeDependent;

	public SingletonDependsOnPrototype(PrototypeDependent prototypeDependent) {
		this.prototypeDependent = prototypeDependent;
	}

	public PrototypeDependent getPrototypeDependent() {
		return prototypeDependent;
	}
}
