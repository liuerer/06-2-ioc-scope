package com.twuc.webApp;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PrototypeDependentWithProxyAnother {

	private static int instanceNum = 0;

	public PrototypeDependentWithProxyAnother() {
		++instanceNum;
	}

	public int getInstanceNum() {
		return instanceNum;
	}
}
