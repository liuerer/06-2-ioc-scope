package com.twuc.webApp;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class SimpleSingletonScopeClass {
	private Date date;

	public SimpleSingletonScopeClass() {
		date = new Date();
	}

	public Date getDate() {
		return date;
	}
}
