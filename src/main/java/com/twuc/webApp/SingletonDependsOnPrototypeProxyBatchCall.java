package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxyBatchCall {
	private PrototypeDependentWithProxyAnother prototypeDependentWithProxyAnother;

	private static int count = 0;

	public SingletonDependsOnPrototypeProxyBatchCall(PrototypeDependentWithProxyAnother prototypeDependentWithProxyAnother) {
		this.prototypeDependentWithProxyAnother = prototypeDependentWithProxyAnother;
		++count;
	}

	public PrototypeDependentWithProxyAnother getPrototypeDependentWithProxyAnother() {
		return prototypeDependentWithProxyAnother;
	}

	public void CallToString(){
		prototypeDependentWithProxyAnother.toString();
		prototypeDependentWithProxyAnother.toString();
	}

	public int getCount() {
		return count;
	}
}
