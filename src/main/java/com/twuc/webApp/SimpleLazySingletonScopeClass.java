package com.twuc.webApp;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Lazy
public class SimpleLazySingletonScopeClass {
	private Date date;

	public SimpleLazySingletonScopeClass() {
		date = new Date();
	}

	public Date getDate() {
		return date;
	}
}
