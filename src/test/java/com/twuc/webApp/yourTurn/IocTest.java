package com.twuc.webApp.yourTurn;

import com.twuc.webApp.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class IocTest {

	private AnnotationConfigApplicationContext context;

	@BeforeEach
	private void init_context() {
		context = new AnnotationConfigApplicationContext("com.twuc.webApp");
	}

	@Test
	void should_compare_object_interface() {
		InterfaceOne bean = context.getBean(InterfaceOne.class);
		InterfaceOne bean2 = context.getBean(InterfaceOneImpl.class);

		assertSame(bean, bean2);
	}

	@Test
	void should_compare_object_class() {
		ParentClass bean = context.getBean(ParentClass.class);
		SonClass bean2 = context.getBean(SonClass.class);

		assertSame(bean, bean2);
	}

	@Test
	void should_compare_object_abstract_class() {
		AbstractBaseClass bean = context.getBean(AbstractBaseClass.class);
		DerivedClass bean2 = context.getBean(DerivedClass.class);

		assertSame(bean, bean2);
	}

	@Test
	void should_compare_object_prototype() {
		SimplePrototypeScopeClass bean = context.getBean(SimplePrototypeScopeClass.class);
		SimplePrototypeScopeClass bean2 = context.getBean(SimplePrototypeScopeClass.class);

		assertNotSame(bean, bean2);
	}

	@Test
	void should_created_before_get_bean_for_singleton() {
		Date date = new Date();
		SimpleSingletonScopeClass bean = context.getBean(SimpleSingletonScopeClass.class);

		assertTrue(bean.getDate().before(date));
	}

	@Test
	void should_created_when_get_bean_for_Prototype() {
		Date date = new Date();
		SimplePrototypeScopeClass bean = context.getBean(SimplePrototypeScopeClass.class);
    Date date1 = bean.getDate();
		assertTrue(bean.getDate().after(date));
	}

	@Test
	void should_created_when_get_bean_for_lazy_singleton() {
		Date date = new Date();
		SimpleLazySingletonScopeClass bean = context.getBean(SimpleLazySingletonScopeClass.class);

		assertTrue(bean.getDate().after(date));
	}

	@Test
	void verify_created_times_prototype_object_on_singleton_dependent() {
		PrototypeScopeDependsOnSingleton bean = context.getBean(PrototypeScopeDependsOnSingleton.class);
		PrototypeScopeDependsOnSingleton bean2 = context.getBean(PrototypeScopeDependsOnSingleton.class);

		assertNotSame(bean, bean2);
		assertSame(bean.getSingletonDependent(), bean2.getSingletonDependent());
	}

	@Test
	void verify_created_times_singleton_object_on_prototype_dependent() {
		SingletonDependsOnPrototype bean = context.getBean(SingletonDependsOnPrototype.class);
		SingletonDependsOnPrototype bean2 = context.getBean(SingletonDependsOnPrototype.class);

		assertSame(bean, bean2);
		assertSame(bean.getPrototypeDependent(), bean2.getPrototypeDependent());
	}

	@Test
	void should_get_new_dependent_instance_when_creat_object() {
		SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
		SingletonDependsOnPrototypeProxy bean2 = context.getBean(SingletonDependsOnPrototypeProxy.class);
		int num1 = bean.getPrototypeDependentWithProxy().getInstanceNum();
		int num2 = bean2.getPrototypeDependentWithProxy().getInstanceNum();

		assertNotEquals(num1, num2);
	}

    @Test
    void should_verify_numbers_of_object_be_created() {
        SingletonDependsOnPrototypeProxyBatchCall bean = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        bean.CallToString();
        int num = bean.getPrototypeDependentWithProxyAnother().getInstanceNum();
        int count = bean.getCount();

        assertEquals(3, num);
        assertEquals(1, count);
	}

    @Test
    void should_be_one_proxy() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);

        assertNotSame(PrototypeDependentWithProxy.class, bean.getPrototypeDependentWithProxy().getClass());
    }
}
